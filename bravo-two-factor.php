<?php
/**
 *
 * @link              www.bravomedia.se
 * @since             1.0.0
 *
 * @wordpress-muplugin
 * Plugin Name:       Bravo Two Factor
 * Plugin URI:        www.bravomedia.se
 * Description:       This is a companionplugin for Two Factor (https://www.wordpress.org/two-factor) to enable Two Factor by default for all administrator roles.
 * Version:           1.3.0
 * Author:            BravoMedia
 * Author URI:        www.bravomedia.se
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Force Two Factor Authentication for administrators.
 *
 * @param array $providers The enabled two-factor providers for the user.
 * @param int $user_id The user ID.
 * @return array The modified list of two-factor providers.
 */
function force_2f_admins( $providers, $user_id ) {
    // Get the list of excluded users, defaulting to user ID 1.
    $excluded_users = apply_filters( 'bravo-two-factor_excluded-users', array( 1 ) );

    // Check if the user can edit posts and is not in the excluded users list.
    if ( user_can( $user_id, 'edit_posts' ) && ! in_array( $user_id, $excluded_users ) ) {
        // If no providers are set and the Two_Factor_Email class exists, add it to the providers list.
        if ( empty( $providers ) && class_exists( 'Two_Factor_Email' ) ) {
            $providers[] = 'Two_Factor_Email';
        }
        // Apply a filter to allow modification of the default providers.
        $providers = apply_filters( 'bravo-two-factor_default-providers', $providers );
        
        // Disable two-factor authentication for non-production environments.
        if ( ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ) {
            $providers = array( 'Two_Factor_Dummy' );
        }
    }
    
    // If the login is a Open ID Login, disable 2FA.
    if ( get_user_meta( $user_id, 'openid-connect-generic-last-id-token-claim', true ) ) {
        $open_id_response = get_user_meta( $user_id, 'openid-connect-generic-last-id-token-claim', true );
        $open_id_response = maybe_unserialize( $open_id_response );
        
        if ( $open_id_response['exp'] > time() - 60 ) {
            return apply_filters( 'bravo-two-factor_openid-providers', array(), $user_id );
        }
    }

    return $providers;
}

add_filter( 'two_factor_enabled_providers_for_user', 'force_2f_admins', 10, 2 );